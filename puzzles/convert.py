puzzle_bank = []

with open("hard.dat") as puzzle_file:
    puzzles = puzzle_file.readlines()

for i, puzzle in enumerate(puzzles):
    puzzles[i] = puzzle.strip()

for line in puzzles:
    split_puzzle = line.split("|")
    temp_puzzle = [[], [], [], [], [], [], [], [], []]
    for s, string in enumerate(split_puzzle):
        temp_puzzle[s] = list(string)
    puzzle_bank.append(temp_puzzle)

with open("bank.txt", "w") as bank_file:
    for puzzle in puzzle_bank:
        bank_file.write(str(puzzle) + "\n")
