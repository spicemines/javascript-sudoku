$(document).ready(function() {
    (function() {
        let gameStartTime; // This variable will hold the time that the board is loaded.
        let gameWon = false; // A flag to indicate when the player has completed a board.
        let puzzleLevel = { // An object used to determine the current puzzle difficulty, or level.
            easy: false,
            medium: false,
            hard: false,
            custom: false // Custom is currently not in use.
        };
        
        /*
         *  This function takes in an array of CSS classes and checks each 
         *    class for errors, namely, the arrays in the puzzleCheck() 
         *    function. This function uses a while loop to loop through the 
         *    array, passing each item to the errorCheck() function and 
         *    assigning that return value to the error variable. If that 
         *    function returns true, the while loop breaks; otherwise, error 
         *    remains false. Finally, the value of error is returned.
         *  @param {array} classArray: an array of CSS classes.
         *  @return {boolean}: true if an error is found in the current class; 
         *    otherwise, false.
         */
        let checkClass = function(classArray) {
            let index = 0;
            let error;
            while (index < classArray.length) {
                error = errorCheck(classArray[index]);
                if (error) {
                    break; // If an error is found, break out of the while loop.
                }
                index += 1;
            }
            return error;
        }; // End checkClass
        
        /*
         *  This function checks each row, column, and block of the puzzle for 
         *    errors, and if no errors are found, it checks the puzzle for 
         *    blank cells. To check for errors, it passes each of the arrays to 
         *    the checkClass() function. If any of those function calls return 
         *    true, errorCode is set to the string "error" to be returned. If 
         *    the error checks all return false, each row is checked for blank 
         *    cells. This is accomplished by iterating through each row class. 
         *    During each iteration, rowElements is set equal to a jQuery 
         *    object containing the items in the current row class. That jQuery 
         *    object is then iterated over, checking the text value of each item 
         *    (i.e., the text value of the <td> element). If that text value is 
         *    an empty string, errorCode is updated to "empty" to be returned. 
         *    Finally, the value of errorCode is returned.
         *  @return {string}: the error code used to determine whether or not 
         *    the player has completed the puzzle or if it has errors.
         */
        this.puzzleCheck = function puzzleCheck() {
            const rows = [".r1", ".r2", ".r3", ".r4", ".r5", ".r6", ".r7", ".r8", ".r9"];
            const columns = [".c1", ".c2", ".c3", ".c4", ".c5", ".c6", ".c7", ".c8", ".c9"];
            const blocks = [".b1", ".b2", ".b3", ".b4", ".b5", ".b6", ".b7", ".b8", ".b9"];
            let rowElements; // Used to store all cells for the current row when checking for blanks.
            let errorCode = "win"; // The player wins if none of the below conditions change this value;
            
            // Check for errors.
            if (checkClass(rows)) {
                errorCode = "error"; // Error found.
            } else if (checkClass(columns)) {
                errorCode = "error"; // Error found.
            } else if (checkClass(blocks)) {
                errorCode = "error"; // Error found.
            } else { // Check for blanks.
                for (i = 0; i < rows.length; i++) {
                    rowElements = $(rows[i]); // Get all elements of the current class.
                    for (j = 0; j < rowElements.length; j++) {
                        if ($(rowElements[j]).text() === "") { // If the text of the current element is an empty string...
                            errorCode = "empty"; // Empty cell found.
                        }
                    }
                }
            }
            return errorCode;
        }; // End puzzleCheck
        
        /*
         *  This function checks a passed in CSS class for errors. It begins by 
         *    creating a jQuery object containing the elements of the given 
         *    class. Then, the jQuery object is iterated over and, if the 
         *    current element does not contain an empty string (i.e., it 
         *    contains a number), the text value of the current element is 
         *    pushed into the numbers array. Then the numbers array is sorted. 
         *    Next, the numbers array is iterated over and, if the current 
         *    number is equal to the next number in the array, this function 
         *    returns true; otherwise, it returns false. Returning true 
         *    indicates that the presence of an error is true.
         *  @param {string} cssClass: the class to be checked for errors.
         *  @return {boolean}: true if an error occurs, false otherwise.
         */
        this.errorCheck = function errorCheck(cssClass) {
            const classPattern = new RegExp("^.*$");
            if (classPattern.test(cssClass)) { // Checking that cssClass begins with '.' and assuming a valid class name after that.
                let elements = $(cssClass);
                let numbers = []; // Used for storing the numbers found in elements.
                for (i = 0; i < elements.length; i++) {
                    if ($(elements[i]).text() !== "") { // If the current element does not contain an empty string...
                        numbers.push($(elements[i]).text()); // Add the number to the numbers array.
                    }
                }
                numbers.sort();
                for (i = 0; i < numbers.length; i++) {
                    if (numbers[i] === numbers[i+1]) { // If the current number is equal to the next one in the array...
                        return true; // An error exists.
                    }
                }
                return false; // No error exists.
            } else {
                console.log(cssClass + " does not match a basic class name pattern for a CSS class.");
            }
        }; // End errorCheck
        
        /*
         *  This is a getter function to access the value of the passed in 
         *    level. It does enforce the parameter level to be a property of 
         *    puzzleLevel.
         *  @param {string} level: the difficulty level to get.
         */
        this.getPuzzleLevel = function getPuzzleLevel(level) {
            if (puzzleLevel.hasOwnProperty(level)) {
                return puzzleLevel[level];
            } else {
                console.log(level + " is not a property of the puzzleLevel object.");
            }
        };
        
        /*
         *  This function determined the amount of time that has passed while 
         *    the player has been playing the current puzzle. It starts by 
         *    getting the current time in seconds. Then, it sets the 
         *    gameDuration variable equal to a sequence of chained actions. 
         *    This sequence begins by subtracting gameStartTime from gameEndTime 
         *    and dividing the difference by 60 to get a decimal value of the 
         *    minutes passed, then converts that decimal number to a string, 
         *    and then splits that string at the decimal point. This results in 
         *    an array of two elements: 1) the number of whole minutes; 2) a 
         *    fraction of a minute. Next, this function prepends a "0." to the 
         *    fraction of a minute (gameDuration[1]) to make it look like a 
         *    decimal value (e.g., 0.5371337). Next, this function builds the 
         *    gameTimeMsg string. The string is built using the number of whole 
         *    minutes (gameDuration[0]) and the resulting decimal value of the 
         *    fraction of a minute rounded down. Finally, gameTimeMsg is 
         *    returned.
         *  @return {string}: a message containing the number of minutes and 
         *    seconds that have passed since the player began playing the 
         *    current puzzle.
         */
        this.getGameTime = function getGameTime() {
            let gameEndTime = new Date().getTime() / 1000; // The current time in seconds.
            let gameDuration = ((gameEndTime - gameStartTime) / 60).toString().split("."); // Game duration in minutes.
            let decimal = "0." + gameDuration[1]; // Make the fraction of a minute look like a decimal value.
            let gameTimeMsg = gameDuration[0] + " minutes and " + Math.floor(parseFloat(decimal) * 60) + " seconds.";
            return gameTimeMsg;
        }; // End getGameTime
        
        /*
         *  This is a getter function for the gameWon variable.
         */
        this.getGameWon = function getGameWon() {
            return gameWon;
        }; // End getGameWon
        
        /*
         *  This function is used to set the difficulty level of the puzzle to 
         *    be played. First, it resets all the properties in the puzzleLevel 
         *    object to false. Second, it sets the property of the passed in 
         *    level to true. It does enforce the parameter level to be a 
         *    property of puzzleLevel.
         *  @param {string} level: the difficulty level.
         */
        this.setPuzzleLevel = function setPuzzleLevel(level) {
            if (puzzleLevel.hasOwnProperty(level)) {
                for (item in puzzleLevel) {
                    puzzleLevel[item] = false; // Reset all levels to false;
                }
                puzzleLevel[level] = true; // Set the desired board level.
            } else {
                console.log(level + " is not a property of the puzzleLevel object.");
            }
        };
        
        /*
         *  This is a setter function for the gameWon variable. It does enforce 
         *    the parameter flag to be of boolean type.
         *  @param {boolean} flag: true or false value.
         */
        this.setGameWon = function setGameWon(flag) {
            if (typeof flag === "boolean") {
                gameWon = flag;
            } else {
                console.log(flag + " is a " + typeof flag + " -- Invalid type. Must be a boolean (true/false).");
            }
        }; // End setGameWon
        
        /*
         *  This function sets the value of gameStartTime to the current time in 
         *    seconds. Normally, the time from the Date object is in 
         *    milliseconds. It is converted to seconds to later determine the 
         *    number of seconds that has passed since the current puzzle was 
         *    loaded.
         */
        this.setStartTime =  function setStartTime() {
            gameStartTime = new Date().getTime() / 1000; // The current time in seconds.
        }; // End setStartTime
    })();
});

