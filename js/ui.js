$(document).ready(function() {
    let savedCell; // This is the most recently clicked cell.
    let savedNoteCell; // This is the most recently clicked cell in the notes table.
    /*
     *  The savedColor variable requires a little documentation. It is used to 
     *    store the color of what the background color of the current cell 
     *    should be if it were not sky blue as assigned by 
     *    indicateSelectedCell(). In other words, when the player clicks on a 
     *    different cell, the previous cell needs to have the proper background 
     *    color restored. For example, if highlighting is enabled, all cells of 
     *    the current row, column, and block are highlighted. If the player 
     *    clicks on a cell that is in the same row, the previous cell background 
     *    would normally go back to white. But, using this savedColor variable, 
     *    the background color of the previous cell will have the proper color 
     *    to match the highlighting.
     */
    let savedColor;
    let highlighting = false; // A flag used to determine if the player has enabled cell highlighting.
    let notes = false; // A flag used to enable and disable the notes feature.
    let keyboardControls = false; // A flag used to allow keyboard controls during a puzzle.
    
    /*
     *  This function is used to clear all notes on the notes table. First, it 
     *    creates a jQuery object containing elements with the notes-text class. 
     *    Then it iterates through that array of objects, and if the text value 
     *    of that object is not an empty string, it sets the text value of the 
     *    current object to an empty string.
     */
    function clearAllNotes() {
        noteCells = $(".notes-text"); // Get all elements with the notes-text class.
        for (i = 0; i < noteCells.length; i++) {
            if ($(noteCells[i]).text() !== "") { // If the text of the current element is not an empty string...
                $(noteCells[i]).text(""); // Set the text of the element to an empty string.
            }
        }
    }
    
    /*
     *  This function is used to clear the current cell. First, it checks to 
     *    see if savedCell is undefined. If so, it prompts the player to click a 
     *    cell. If savedCell is not undefined, this event calls 
     *    resetTableStyles(), sets the text of savedCell to an empty string, 
     *    updates savedColor,  calls highlightCells(), calls lookForErrors(), 
     *    and finally calls indicateSelectedCell().
     */
    function clearCell() {
        if (savedCell === undefined) { // If a cell has not been clicked...
            alert("Please select a cell to clear."); // Alert the player.
        } else { // If a cell is currently clicked...
            resetTableStyles(); // Reset the table color scheme.
            $(savedCell).text(""); // Set savedCell text to an empty string.
            savedColor = $(savedCell).css("background-color"); // Update savedColor.
            highlightCells(); // If applicable, highlight the appropriate cells.
            lookForErrors(); // If any errors exist, highlight the appropriate classes of cells.
            indicateSelectedCell(); // Set the background color of the current cell.
        }
    }
    
    /*
     *  This function is used to clear the contents of a cell on the notes 
     *    table.
     */
    function clearNoteCell() {
        if (savedNoteCell === undefined) { // If a cell has not been clicked...
            alert("Please select a cell in the notes table to clear."); // Alert the player.
        } else { // If a cell is currently clicked...
            $(savedNoteCell).find(".notes-text").text(""); // Clear the contents of the cell.
        }
    }
    
    /*
     *  This function is used to add numbers to a cell in the notes table. When 
     *    called, this function checks to see if a cell has been clicked. If a 
     *    cell has not been clicked, the player is alerted. If a cell has been 
     *    clicked, this function first verifies that number is a string 
     *    (failure prints an error message to the console). The process of 
     *    adding notes to the cell begins by checking to see if text is not an 
     *    empty string. If so, the text string is split at each space character, 
     *    making an array of numbers. Then if number is not in that array, 
     *    number is added to the array and the array is sorted. If number is 
     *    already in the array, then it is removed from the array. Next, the 
     *    array is iterated over and each element is concatenated to noteString, 
     *    each item separated by a space. Next, the text value of the child 
     *    element of savedNoteCell with the notes-text class is assigned the 
     *    value of noteString. Finally, the existingNumbers array is emptied so 
     *    that each call of this function begins with an empty array.
     *  @param {string} text: the text value of the current cell.
     *  @param {string} number: the number to be entered into the cell. This 
     *    must be a string.
     */
    function enterNote(text, number) {
        if (savedNoteCell === undefined) { // If a cell has not been clicked...
            alert("Please select a cell in the notes table to enter notes."); // Alert the player.
        } else { // If a cell is currently clicked...
            if (typeof number === "string") { // Number must be a string for this to work.
                let existingNumbers = []; // This array will store the numbers to be displayed in the cell.
                let notesString = ""; // This will be a string containing the notes to be added to the cell.
                let index; // A variable used to splice the array if number exists in the array.
                if (text !== "") { // If text is not an empty string...
                    existingNumbers = text.split(" "); // Split text at each space character.
                }
                if (existingNumbers.indexOf(number) === -1) { // If number is not in the array...
                    existingNumbers.push(number); // Add number to the array.
                    existingNumbers.sort(); // Sort the array.
                } else { // If number is in the array...
                    index = existingNumbers.indexOf(number); // Get the index of number in the array.
                    existingNumbers.splice(index, 1); // Splice the array at index, removing number from the array.
                }
                for (i = 0; i < existingNumbers.length; i++) {
                    notesString += existingNumbers[i] + " "; // Concatenate each number to the string, separated by a space.
                }
                $(savedNoteCell).find(".notes-text").text(notesString); // Put the numbers into the cell.
                existingNumbers.splice(0, existingNumbers.length); // This line makes sure the array is emptied after use.
            } else { // If number is not a string...
                console.log(number + " is a " + typeof number + " -- Invalid type. Must be a string.");
            }
        }
    }
    
    /*
     *  This function is used to enter a number into the current cell. First, 
     *    it checks to see if savedCell is undefined. If so, it prompts the 
     *    player to click a cell. If savedCell is not undefined, this event 
     *    calls resetTableStyles(), sets the text of savedCell to the value of 
     *    the number parameter, calls highlightCells(), calls lookForErrors(), 
     *    and finally calls indicateSelectedCell().
     *  @param {number|string} number: the number to be entered into the currently 
     *    selected cell.
     */
    function enterNumber(number) {
        if (savedCell === undefined) { // If a cell has not been clicked...
            alert("Please select a cell to enter a number."); // Alert the player.
        } else { // If a cell is currently clicked...
            resetTableStyles(); // Reset the table color scheme.
            $(savedCell).text(number); // Set savedCell text to the value of number.
            highlightCells(); // If applicable, highlight the appropriate classes of cells.
            lookForErrors(); // If any errors exist, highlight the appropriate class of cells.
            indicateSelectedCell(); // Set the background color of the current cell.
        }
    }
    
    /*
     *  This function gets the value of the class attribute of savedCell 
     *    (savedCell is a <td> element). Since the class attribute contains a 
     *    space-separated list of classes, it is split at each space. Then, 
     *    each element of the resulting array has a "." prepended to it to look 
     *    like a CSS class selector. Then, each of those class selectors is 
     *    packed into an array. Finally, the classes array is returned.
     *  @return {array}: an array containing each class of savedCell.
     */
    function getCellClasses() {
        let cellClasses = $(savedCell).attr("class").split(" "); // Get the class attribute for the current cell and split it at the spaces.
        let rowClass = "." + cellClasses[0]; // Row class;
        let colClass = "." + cellClasses[1]; // Column class.
        let blockClass = "." + cellClasses[2]; // Block class;
        let classes = [rowClass, colClass, blockClass]; // Index: 0) row class; 1) column class; 3) block class.
        return classes;
    }
    
    /*
     *  This function highlights the matching row, column, and block of the 
     *    currently selected cell. First, it checks to see if highlighting is 
     *    set to true. If so, it gets an array of the appropriate classes from 
     *    getCellClasses(), changes the background color property to the 
     *    highlight color, and then updates the savedColor.
     */
    function highlightCells() {
        if (highlighting) {
            let classes = getCellClasses();
            $(classes[0]).css("background-color", "#c8e37f"); // Highlight the current row.
            $(classes[1]).css("background-color", "#c8e37f"); // Highlight the current column.
            $(classes[2]).css("background-color", "#c8e37f"); // Highlight the current block.
            savedColor = "#c8e37f"; // Update the saved color;
        }
    }
    
    /*
     *  This function simply changes the background color property of the 
     *    currently selected cell to sky blue.
     */
    function indicateSelectedCell() {
        $(savedCell).css("background-color", "skyblue");
    }
    
    /*
     *  This function changes the background color property of the current row, 
     *    column, or block based on the return value of the errorCheck() 
     *    function (in gamecontroller.js).
     */
    function lookForErrors() {
        let classes = getCellClasses(); // Get the classes of the currently selected cell.
        if (errorCheck(classes[0])) { // Check the current row for errors.
            $(classes[0]).css("background-color", "#eb8691"); // If an error is found, change the background color.
            savedColor = "#eb8691"; // Update savedColor.
        } else if (errorCheck(classes[1])) { // Check the current column for errors.
            $(classes[1]).css("background-color", "#eb8691"); // If an error is found, change the background color.
            savedColor = "#eb8691"; // Update savedColor.
        } else if (errorCheck(classes[2])) { // Check the current block for errors.
            $(classes[2]).css("background-color", "#eb8691"); // If an error is found, change the background color.
            savedColor = "#eb8691"; // Update savedColor.
        }
    }
    
    /*
     *  This function is used to reset the game board to its default color 
     *    scheme. It is called when a class of cells has changed color and that 
     *    color is no longer needed.
     */
    function resetTableStyles() {
        $("#sudoku-puzzle td").css("background-color", "white");
        $(".no-edit").css("background-color", "#eee");
    }
    
    /*
     *  This function performs some preparation for the puzzle when the player 
     *    begins a new puzzle. For instance, if the controls have been disabled 
     *    (as when the player completes a puzzle), the controls are re-enabled 
     *    by removing the disabled attribute. Finally, the setStartTime() 
     *    function is called to update the value of gameStart (in 
     *    gamecontroller.js) to the current time.
     */
    function setUpPuzzle() {
        if ($("#btn-new").siblings("button").attr("disabled") === "disabled") {
            $("#btn-new").siblings("button").removeAttr("disabled")
        }
        setGameWon(false); // The game has not yet been won. It has only begun!
        keyboardControls = true; // Enable keyboard controls.
        setStartTime();
    }
    
    /*
     *  This function toggles cell highlighting. First, it checks to see if 
     *    savedCell is undefined. If so, it prompts the player to click a cell. 
     *    If savedCell is not undefined, it checks the value of highlighting. 
     *    If highlighting is false, highlighting is set to true, the appearance 
     *    of the button is changed to indicate activation, highlightCells() is 
     *    called, and then indicateSelectedCell() is called. If highlighting is 
     *    true, highlighting is set to false, the button appearance is changed 
     *    back to the default, resetTableStyles() is called, and then 
     *    indicateSelectedCell() is called.
     */
    function toggleHighlighting() {
        if (savedCell === undefined) { // If a cell has not been clicked...
            alert("Please select a cell to enable highlighting."); // Alert the player.
        } else { // If a cell is currently clicked...
            if (!highlighting) { // If highlighting is currently disabled...
                highlighting = true; // Enable highlighting.
                $("#btn-highlight").css("color", "#517f8f"); // Change the text color of the Highlighting button.
                highlightCells(); // Highlight the appropriate classes of cells.
                indicateSelectedCell(); // Set the background color of the current cell.
            } else { // If highlighting is currently enabled...
                highlighting = false; // Disable highlighting.
                $("#btn-highlight").css("color", "#23363d"); // Change the text color of the Highlighting button.
                resetTableStyles(); // Reset the table color scheme.
                indicateSelectedCell(); // Set the background color of the current cell.
            }
        }
    }
    
    /*
     *  This function toggles the note taking feature. First off, it checks to 
     *    see if notes is false, and if it is, it sets notes to true, changes 
     *    the color of the Notes button, and then reveals the notes table. If 
     *    notes is true, this function sets notes to false, changes the color 
     *    of the Notes button back to the original, and then hides the notes 
     *    table.
     */
    function toggleNotes() {
        if (!notes) { // If notes are disabled...
            notes = true; // Enable notes.
            $("#btn-notes").css("color", "#517f8f"); // Change the text color of the Notes button.
            $("#notes-container").slideDown(250); // Reveal the notes table.
        } else { // If notes are enabled...
            notes = false; // Disable notes.
            $("#btn-notes").css("color", "#23363d"); // Change the text color of the Notes button.
            $("#notes-container").slideUp(250); // Hide the notes table.
        }
    }
    
    /*
     *  This event handles keyboard functionality in the game. When one of the 
     *    keys noted in the switch block is pressed, the appropriate action is 
     *    taken. However, the keyboard controls here are only used if the 
     *    keyboardControls flag is set to true. If this check were not made, 
     *    then there would be a keyboard conflict on the menu screen because 
     *    the number keys are needed for specifying a custom puzzle file name. 
     *    Without this check, every time the player typed in a number, the 
     *    letter 'c', or the Delete key, an alert would pop up notifying the 
     *    player to click on a cell.
     */
    $(document).keydown(function(event) {
        if (keyboardControls) { // If the keyboard controls are enabled...
            switch (event.which) {
                case 49: // Number 1 key pressed.
                case 97: // 1 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "1");
                    } else {
                        enterNumber(1);
                    }
                    break;
                case 50: // Number 2 key pressed.
                case 98: // 2 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "2");
                    } else {
                        enterNumber(2);
                    }
                    break;
                case 51: // Number 3 key pressed.
                case 99: // 3 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "3");
                    } else {
                        enterNumber(3);
                    }
                    break;
                case 52: // Number 4 key pressed.
                case 100: // 4 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "4");
                    } else {
                        enterNumber(4);
                    }
                    break;
                case 53: // Number 5 key pressed.
                case 101: // 5 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "5");
                    } else {
                        enterNumber(5);
                    }
                    break;
                case 54: // Number 6 key pressed.
                case 102: // 6 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "6");
                    } else {
                        enterNumber(6);
                    }
                    break;
                case 55: // Number 7 key pressed.
                case 103: // 7 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "7");
                    } else {
                        enterNumber(7);
                    }
                    break;
                case 56: // Number 8 key pressed.
                case 104: // 8 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "8");
                    } else {
                        enterNumber(8);
                    }
                    break;
                case 57: // Number 9 key pressed.
                case 105: // 9 on the number pad was pressed.
                    if (notes) {
                        enterNote($(savedNoteCell).find(".notes-text").text(), "9");
                    } else {
                        enterNumber(9);
                    }
                    break;
                case 46: // Delete key pressed.
                case 48: // Number 0 key pressed.
                case 67: // Letter 'C' key pressed.
                case 96: // 0 on the number pad was pressed.
                case 110: // '.' on the number pad was pressed.
                    if (notes) {
                        clearNoteCell();
                    } else {
                        clearCell();
                    }
                    break;
                case 72: // Letter 'H' key pressed.
                    toggleHighlighting();
                    break;
                case 78: // Letter 'N' key pressed.
                    toggleNotes();
                    break;
                default: // Don't do anything for any other key pressed.
                    break;
            }
        }
    });
    
    /*
     *  This event occurs when the user clicks on a cell in the puzzle. The 
     *    $(document.on() method is used because if it were a normal 
     *    $("#sudoku-puzzle td").click() method, any newly loaded puzzle would 
     *    not gain this click functionality. Attaching the click event to the 
     *    "#sudoku-puzzle td" selector via the document ensures that any newly 
     *    loaded elements will have the click event.
     */
    $(document).on("click", "#sudoku-puzzle td", function(event) {
        if (!$(this).hasClass("no-edit")) { // If the clicked cell can be edited...
            $(savedCell).css("background-color", savedColor); // Set the previous cell's background to its original value.
            savedColor = $(this).css("background-color"); // Update the saved color.
            resetTableStyles(); // Reset the table color scheme.
            savedCell = $(this); // Update the current cell.
            highlightCells(); // If necessary, highlight the appropriate cells.
            lookForErrors(); // Check the puzzle for errors.
            $(this).css("background-color", "skyblue"); // Set the newly clicked cell's background color.
        }
    });
    
    /*
     *  This is the click event for cells in the notes table.
     */
    $("#notes-table td").click(function() {
        $(savedNoteCell).css("background-color", "rgba(192, 192, 192, 0.3)"); // Set the previous cell's background to its original value.
        savedNoteCell = $(this); // Update the current cell.
        $(this).css("background-color", "rgba(135, 206, 235, 0.4)"); // Set the newly clicked cell's background color.
    });
    
    /*
     *  This is the click event for the number buttons. When fired, if notes 
     *    are enabled it passes the text value of the button clicked to the 
     *    enterNote() function; otherwise it passes the text value of the 
     *    button clicked to the enterNumber() function.
     */
    $("button.num").click(function() {
        if (notes) {
            enterNote($(savedNoteCell).find(".notes-text").text(), $(this).text());
        } else {
            enterNumber($(this).text());
        }
    });
    
    /*
     *  This is the click event for the Clear button. When fired, if notes are 
     *    enabled, it calls clearNoteCell(); otherwise, it calls the clearCell() 
     *    function.
     */
    $("#btn-clear").click(function() {
        if (notes) {
            clearNoteCell();
        } else {
            clearCell();
        }
    });
    
    /*
     *  This is the click event for the Check Board button. When fired, it calls 
     *    puzzleCheck() (in gamecontroller.js) to check the puzzle for errors. 
     *    Then, depending on the return value of puzzleCheck(), the user is 
     *    prompted with a message. If the puzzle contains no errors when 
     *    clicking this button, "win" is returned and this event does a few 
     *    finishing up things. Finishing up, it begins by getting the amount of 
     *    time the player has been playing the current puzzle, then it alerts 
     *    the player of success, resets the table styles, disables 
     *    highlighting, disables keyboard controls, takes care of notes, and 
     *    finally disables all sibling buttons to #btn-new so that the player 
     *    can load a new puzzle.
     */
    $("#btn-check").click(function() {
        let puzzleStatus = puzzleCheck(); // Check the puzzle for errors or blank cells.
        if (puzzleStatus === "empty") { // If a blank cell is found...
            alert("The puzzle is not yet cmplete.");
        } else if (puzzleStatus === "error") { // If an error is found...
            alert("The puzzle contains errors.");
        } else if (puzzleStatus === "win") { // If no blank cells and no errors, the player wins.
            var gameTime = getGameTime(); // Get the time it took the player to complete the puzzle.
            alert("Congratulations! You completed the puzzle.\n\nYour time: " + gameTime); // Congratulatory statement with the time.
            setGameWon(true);
            resetTableStyles(); // Reset the table color scheme.
            highlighting = false; // Disable highlighting.
            keyboardControls = false; // Disable keyboard controls.
            if (notes) { // If notes is true...
                toggleNotes();
            }
            clearAllNotes(); // Clear all existing notes.
            $("#btn-new").siblings("button").attr("disabled", "disabled"); // Disable the appropriate buttons.
        }
    });
    
    /*
     *  This is the click event for the Notes button. When fired, it calls the 
     *    toggleNotes() function.
     */
    $("#btn-notes").click(function() {
        toggleNotes();
    });
    
    /*
     *  This is the click event for the Highlighting button. When fired, it 
     *    calls the toggleHighlighting() function.
     */
    $("#btn-highlight").click(function() {
        toggleHighlighting();
    });
    
    /*
     *  This is the click event for the Load Board button. When fired, it 
     *    uses the current puzzle level to get the value of the corresponding
     *    select element and assigns that value to the selectedPuzzle variable.
     *    With the puzzle selected, a check is performed to make sure the
     *    puzzle exists in the puzzles object from the puzzles.js file. If the
     *    puzzle exists, a for loop nested within another populates each cell
     *    of the puzzle with the corresponding value in the puzzle array. Once
     *    the puzzle is populated, setUpPuzzle() is called.
     */
    $("#btn-load").click(function() {
        $("td").removeClass("no-edit"); // Clear out any previously non-editable cells.
        // This string will contain the property from the puzzles object that will be the puzzle to solve.
        let selectedPuzzle = "";
        // This array is used to check if a value going into a cell is a number. I have tried the
        //   JS isNaN() function, but it doesn't work here. Also, if either 0 or 9 is missing from
        //   this array, 9s are not included in the number check.
        const numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        if (getPuzzleLevel("easy")) { // Easy puzzles.
            selectedPuzzle = $("#easy-puzzles").val();
        } else if (getPuzzleLevel("medium")) { // Medium puzzles.
            selectedPuzzle = $("#medium-puzzles").val();
        } else if (getPuzzleLevel("hard")) { // Hard puzzles.
            selectedPuzzle = $("#hard-puzzles").val();
        } else { // No puzzle was selected.
            alert("Please select a puzzle.");
            return;
        }
        if (selectedPuzzle in puzzles) { // If the puzzle is found in the puzzles.js file...
            for (row=0; row<9; row++) { // Iterate through the rows.
                rowNum = row + 1; // Add 1 to match the CSS class numbering used in the HTML.
                let rowClass = ".r" + rowNum; // Complete the class name.
                for (cell=0; cell<9; cell++) { // Iterate through each cell in the current row.
                    let colNum = cell + 1; // Add 1 to match the CSS class numbering used in the HTML
                    let colClass = ".c" + colNum; // Complete the class name.
                    let classNames = rowClass + colClass; // Combine the classes to build the CSS selector string.
                    $(classNames).text(puzzles[selectedPuzzle][row][cell]); // Insert the current number.
                    // If the current cell is a number string and it exists in the original puzzle, add the
                    //   no-edit CSS class to prevent those cells from being edited during the game.
                    if (puzzles[selectedPuzzle][row][cell] in numbers) {
                        $(classNames).addClass("no-edit");
                    }
                }
            }
        } else {
            alert("Puzzle not found. Please select another puzzle.");
            return;
        }
        $("#menu-container").slideUp(500, function() { // Change the interface to show the new puzzle and controls.
            $("#game-area").fadeIn(250, function() {
            $("#controls-container").fadeIn(250)
            });
        });
        // Change the title of the puzzle to the text in the option element that matches the value of selectedPuzzle.
        //   This is achieved by building a jQuery attribute selector string to get the text from the selected
        //   option element with a value attribute equal to selectedPuzzle.
        $("#puzzle-title").text($("[value=" + selectedPuzzle + "]").text())
        resetTableStyles(); // Reset the table color scheme.
        setUpPuzzle(); // Run the puzzle setup.
    });

    /*
     *  This is the click event for the New Board button. When fired, it checks 
     *    to see if the player has completed the puzzle, and if not, it has the 
     *    player confirm that a new puzzle is to be loaded, If the player has 
     *    completed the puzzle or confirms to load a new puzzle, then the 
     *    current puzzle and controls are hidden, notes are disabled and 
     *    cleared, and the menu is shown.
     */
    $("#btn-new").click(function() {
        let newPuzzle = true; // True by default.
        if (!getGameWon()) { // If the puzzle is not complete...
            newPuzzle = confirm("Are you sure you want to load a new puzzle?\n\nProgress on this puzzle will be lost.");
        }
        if (newPuzzle) { // If a new puzzle is to be loaded...
            keyboardControls = false; // Disable keyboard controls so that there is no keyboard conflict on the menu.
            if (notes) { // If notes is true...
                toggleNotes();
            }
            clearAllNotes(); // Clear all existing notes.
            $("#controls-container").fadeOut(250, function() {
                $("#game-area").fadeOut(250, function() {
                    $("#menu-container").slideDown(500);
                })
            });
        }
    });
    
    /*
     *  This is the click event for the Blank Puzzle button. When fired, it 
     *    loads a blank puzzle (custom000). It mainly consists of a for loop
     *    nested withing another that populates each cell of the puzzle with
     *    an empty string.
     */
    $("#btn-blank").click(function() {
        for (row=0; row<9; row++) { // Iterate through the rows.
            rowNum = row + 1; // Add 1 to match the CSS class numbering used in the HTML.
            let rowClass = ".r" + rowNum; // Complete the class name.
            for (cell=0; cell<9; cell++) { // Iterate through each cell in the current row.
                let colNum = cell + 1; // Add 1 to match the CSS class numbering used in the HTML
                let colClass = ".c" + colNum; // Complete the class name.
                let classNames = rowClass + colClass; // Combine the classes to build the CSS selector string.
                // Populate the cells with the blank puzzle, custom000.
                $(classNames).text(puzzles["custom000"][row][cell])
            }
        }
        $("#menu-container").slideUp(500, function() { // Change the interface to show the new puzzle and controls.
            $("#game-area").fadeIn(250, function() {
                $("#controls-container").fadeIn(250)
            });
        });
        setUpPuzzle(); // Run the puzzle setup.
    });
    
    /*
     *  This is the click event for the How to Play button.
     */
    $("#btn-howtoplay").click(function() {
        $("#menu-container").slideUp(500, function() {
            $("#howtoplay-container").slideDown(500);
        });
    });
    
    /*
     *  This is the click event for Return buttons.
     */
    $(".btn-return").click(function() {
        $(this).parent().parent().slideUp(500, function() {
            $("#menu-container").slideDown(500);
        });
    });
    
    /*
     *  This is the click event for the Easy button. When fired, it provides a 
     *    drop-down list of the easy difficulty puzzles available for the player 
     *    to play.
     */
    $("#btn-easy").click(function() {
        if (!getPuzzleLevel("easy")) {
            setPuzzleLevel("easy");
            $(this).siblings("button").css("color", "#23363d");
            $(this).css("color", "#517f8f"); // Change the text color of the button.
            $("#medium-puzzles, #hard-puzzles, #custom-puzzles").slideUp(0, function() {
                $("#easy-puzzles").slideDown(250);
            })
        }
    });
    
    /*
     *  This is the click event for the Medium button. When fired, it provides a 
     *    drop-down list of the medium difficulty puzzles available for the 
     *    player to play.
     */
    $("#btn-medium").click(function() {
        if (!getPuzzleLevel("medium")) {
            setPuzzleLevel("medium");
            $(this).siblings("button").css("color", "#23363d");
            $(this).css("color", "#517f8f"); // Change the text color of the button.
            $("#easy-puzzles, #hard-puzzles, #custom-puzzles").slideUp(0, function() {
                $("#medium-puzzles").slideDown(250);
            })
        }
    });
    
    /*
     *  This is the click event for the Hard button. When fired, it provides a 
     *    drop-down list of the hard difficulty puzzles available for the player 
     *    to play.
     */
    $("#btn-hard").click(function() {
        if (!getPuzzleLevel("hard")) {
            setPuzzleLevel("hard");
            $(this).siblings("button").css("color", "#23363d");
            $(this).css("color", "#517f8f"); // Change the text color of the button.
            $("#easy-puzzles, #medium-puzzles, #custom-puzzles").slideUp(0, function() {
                $("#hard-puzzles").slideDown(250);
            })
        }
    });
});

