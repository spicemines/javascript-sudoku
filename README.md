# Just Another Sudoku Game

A Sudoku game written in HTML, CSS, and JavaScript.

I wrote this game many years ago as a way to practice and work on my HTML, CSS, and JavaScript skills. Recently, I went through the code and updated several things, added more features, and added many more puzzles.

This Sudoku game comes with 300 puzzles and a blank puzzle board that can be used for designing new puzzles or solving puzzles from books, newspapers, or websites. There are three difficulty levels: easy, medium, and hard. Each difficulty level has 100 puzzles.
